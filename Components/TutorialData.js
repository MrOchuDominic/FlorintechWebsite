// import logo from "../assets/webdevelopment.jpg";

// import advancedgraphicsdesign from "./../pages/courses/advancedgraphicsdesign";
export const videoCon = [
  {
    img: "rfmJI8BqxC8&list=PL0EqspLrOAxRX_IeXFX1jcaqHKCc45IJx",
    to: "./../pages/courses/advancegraphicsdesign",
    title: "Graphics Designs  Tutorial for Beginners",
    paragraph:
      " This video summaries  how to become a proffesional graphics designer.",
    textbtn: "view course",
  },
  {
    img: "krKmmuPMJbw",
    to: "./../pages/courses/desktoppublishing",
    title: "Introduction to Desktop Publishing",
    paragraph:
      " Desktop publishing (DTP) is the creation of documents using page layout software on a personal desktop computer",
    textbtn: "view course",
  },
  {
    img: "qNbX1qIz1uQ",
    to: "./../pages/courses/OfficeApplication",
    title: "Office Application  for Beginners",
    paragraph:
      " Office Application  is a Software that is used in business such as word processing.",
    textbtn: "view course",
  },
  {
    img: "kqtD5dpn9C8",
    to: "./../pages/courses/ComputerProgramming",
    title: "Python for Beginners",
    paragraph:
      " Python is an interpreted, object-oriented, high-level programming language with dynamic semanticsn.",
    textbtn: "view course",
  },
  {
    img: "EceJQ05KTf4&list=PLwoh6bBAszPrNlrMqJXnb9G6MdgSfN686",
    to: "./../pages/courses/webdevelopment",
    title: "Introduction to Web Development",
    paragraph:
      " Web development is the building and maintenance of websites; it's the work that happens behind the scenes to make a website look great.",
    textbtn: "view course",
  },
  {
    img: "qoSksQ4s_hg&list=PL4cUxeGkcC9i9Ae2D9Ee1RvylH38dKuET",
    to: "./../pages/courses/webdevelopment",
    title: "Introduction To JavaScript",
    paragraph:
      "JavaScript (js) is a light-weight object-oriented programming language which is used by several websites for scripting the webpages.",
    textbtn: "view course",
  },
];
